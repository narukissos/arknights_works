# Contingency Contract #3 Seasonal Permanent Site Results

# MAP

![map](./img/maps.PNG)

# Results

## Risk 0

Clear Date: 2021/05/29 03:12:31

![0](./img/0.PNG)

## Risk 8

Clear Date: 2021/05/29 04:02:20

![8](./img/8.PNG)

## Risk 15

Clear Date: 2021/06/09 23:46:05

![14](./img/14.PNG)

## Risk 16

Clear Date: 2021/06/10 00:15:38

![16](./img/16.PNG)

# 特別任務

## 任務1
![challenge1](./img/Challenge_1.PNG)

## 任務2
![challenge2](./img/Challenge_2.PNG)

## 任務3
![challenge3](./img/Challenge_3.PNG)

## 任務4
![challenge4](./img/Challenge_4.PNG)

## 任務5
![challenge5](./img/Challenge_5.PNG)

## 任務6
![challenge6](./img/Challenge_6.PNG)

## 任務7
![challenge7](./img/Challenge_7.PNG)

## 任務8
![challenge8](./img/Challenge_8.PNG)