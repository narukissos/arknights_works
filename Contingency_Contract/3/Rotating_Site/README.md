# Contingency Contract #3 Rotating Site Results

## Day1-2

Stage: 崩壊した大通り

Clear Date: 2021/05/29 03:05:34

![day1-2](./img/Day_1-2.PNG)

## Day3

Stage: 第6区跡

Clear Date: 2021/05/50 20:17:10

![day3_8](./img/Day_3.PNG)

## Day4

Stage: 中継所

Clear Date: 2021/05/31 23:25:28

![day4_8](./img/Day_4.PNG)

## Day5

Stage: 廃工場

Clear Date: 2021/06/01 22:48:41 

![day5_8](./img/Day_5.PNG)

## Day6

Stage: 凍てつく廃墟

Clear Date: 2021/06/02 21:38:30

![day6_8](./img/Day_6.PNG)

## Day7

Stage: 黄鉄の峡谷

Clear Date: 2021/06/03 22:41:20

![day7_8](./img/Day_7.PNG)

## Day8
Stage: 武器庫東

Clear Date: 2021/06/05 01:48:45

![day8_8](./img/Day_8.PNG)

## Day9
Stage: 崩壊した大通り

Clear Date: 2021/06/05 10:02:07

![day9_8](./img/Day_9.PNG)

## Day10
Stage: 第6区跡

Clear Date: 2021/06/06 22:06:21

![day10_8](./img/Day_10.PNG)

## Day11
Stage: 中継所

Clear Date: 2021/06/07 20:41:44

![Day_11](./img/Day_11.PNG)

## Day12
Stage: 廃工場

Clear Date: 2021/06/08 20:33:36

![day12_8](./img/Day_12.PNG)

## Day13
Stage: 黄鉄の峡谷

Clear Date: 2021/06/09 21:49:17

![day13_8](./img/Day_13.PNG)

## Day14
Stage: 凍てつく廃墟

Clear Date: 2021/06/20 21:56:16

![day14_8](./img/Day_14.PNG)
