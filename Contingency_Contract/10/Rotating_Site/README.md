# Contingency Contract #10 Rotating Site Results

## Day1-2

Stage: 砂海の遺跡

Clear Date: 2022/08/30 18:29:14

![day1-2](./img/Day_1-2.PNG)

## Day3

Stage: 無秩序な鉱区

Clear Date: 2022/09/01 13:45:34

![day3](./img/Day_3.PNG)

## Day4

Stage: 狂風の砂原

Clear Date: 2022/09/02 12:13:36

![day4](./img/Day_4.PNG)

## Day5

Stage: 灼熱の鍾乳洞

Clear Date: 2022/09/03 20:03:23 

![day5](./img/Day_5.PNG)

## Day6

Stage: 8号競技場

Clear Date: 2022/09/04 11:25:17

![day6](./img/Day_6.PNG)

## Day7

Stage: 棄てられし区画

Clear Date: 2022/09/05 14:18:28

![day7_8](./img/Day_7.PNG)

## Day8
Stage: 灰斉山麓

Clear Date: 2022/09/06 17:20:41

![day8_8](./img/Day_8.PNG)

## Day9
Stage: 狂風の砂原

Clear Date: 2022/09/07 15:35:59

![day9_8](./img/Day_9.PNG)

## Day10
Stage: 灰斉山麓

Clear Date: 2022/09/08 12:04:51

![day10_8](./img/Day_10.PNG)

## Day11
Stage: 灼熱の鍾乳洞

Clear Date: 2022/09/09 11:31:05

![Day_11](./img/Day_11.PNG)

## Day12
Stage: 棄てられし区画

Clear Date: 2022/09/10 21:37:54

![day12_8](./img/Day_12.PNG)

## Day13
Stage: 8号競技場

Clear Date: 2022/09/11 20:25:48

![day13_8](./img/Day_13.PNG)

## Day14
Stage: 砂海の遺跡

Clear Date: 2022/09/12 15:14:47

![day14_8](./img/Day_14.PNG)
