# Contingency Contract #1 Seasonal Permanent Site Results

## Risk 7

Clear Date: 2020/11/11 20:00:36

![7](./img/7.PNG)

## Risk 8

Clear Date: 2020/11/11 21:12:10

![8](./img/8.PNG)

## Risk 10

Clear Date: 2020/11/11 21:21:20

![10](./img/10.PNG)

## Risk 12

Clear Date: 2020/11/11 21:27:10

![12-1](./img/12.PNG)



## Risk 14

Clear Date: 2020/11/11 22:19:14

![14-1](./img/14-1.PNG)

![14-2](./img/14-2.PNG)

## Risk15

Clear Date: 2020/11/13 17:11:37

![15-1](./img/15-1.PNG)

![15-2](./img/15-2.PNG)

## Risk16

Clear Date: 2020/11/13 17:15:38

![16-1](./img/16-1.PNG)

![16-2](./img/16-2.PNG)

## Risk17

Clear Date: 2020/11/13 19:20:44

![17-1](./img/17-1.PNG)

![17-2](./img/17-2.PNG)

## Risk18

Clear Date: 2020/11/19 00:24:47

![18-1](./img/18-1.PNG)

![18-2](./img/18-2.PNG)