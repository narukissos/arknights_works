# Contingency Contract #1 Rotating Site Results

## Day1-2

Stage: 武器庫東

Clear Date: 2020/11/11 19:20:05

![day1-2](./img/Day_1-2.PNG)

## Day3

Stage: 乾荒原

Clear Date: 2020/11/13 15:41:47

![day3_8](./img/Day_3.PNG)

## Day4

Stage: 崩壊した大通り

Clear Date: 2020/11/14

**画像なし**

## Day5

Stage: 新通り

Clear Date: 2020/11/15 

**画像なし**

## Day6

Stage: 中継所

Clear Date: 2020/11/16

**画像なし**

## Day7

Stage: 凍てつく廃墟

Clear Date: 2020/11/17 12:46:36

![day7_8](./img/Day_7.PNG)

## Day8
Stage: 第59区跡

Clear Date: 2020/11/18 10:48:15

![day8_8](./img/Day_8.PNG)

## Day9
Stage: 武器庫東

Clear Date: 2020/11/19 17:05:34

![day9_8](./img/Day_9.PNG)

## Day10
Stage: 乾荒原

Clear Date: 2020/11/20 12:43:39

![day10_8](./img/Day_10.PNG)

## Day11
Stage: 崩壊した大通り

Clear Date: 2020/11/21 14:53:40

![Day_11](./img/Day_11.PNG)

## Day12
Stage: 新通り

Clear Date: 2020/11/22 09:24:45

![day12_8](./img/Day_12.PNG)

## Day13
Stage: 中継所

Clear Date: 2020/11/23 10:48:52

![day13_8](./img/Day_13.PNG)

## Day14
Stage: 凍てつく廃墟

Clear Date: 2020/11/24 12:38:44

![day14_8](./img/Day_14.PNG)