# Contingency Contract #6 Rotating Site Results

## Day1-2

Stage: 第6区跡

Clear Date: 2022/03/02 12:08:24

![day1-2](./img/Day_1-2.PNG)

## Day3

Stage: 棄てられし区画

Clear Date: 2022/03/03 12:12:58

![day3_8](./img/Day_3.PNG)

## Day4

Stage: 廃工場

Clear Date: 2022/03/04 12:12:58

![day4_8](./img/Day_4.PNG)

## Day5

Stage: 閉鎖監獄

Clear Date: 2022/03/05 16:32:07 

![day5_8](./img/Day_5.PNG)

## Day6

Stage: 8号競技場

Clear Date: 2022/03/06 10:45:41

![day6_8](./img/Day_6.PNG)

## Day7

Stage: 風蝕の高原

多分やっていない．．．

## Day8
Stage: 第6区跡

Clear Date: 2022/03/08 12:08:21

![day8_8](./img/Day_8.PNG)

## Day9
Stage: 廃工場

Clear Date: 2022/03/09 14:18:52

![day9_8](./img/Day_9.PNG)

## Day10
Stage: 棄てられし区画

Clear Date: 2022/03/10 11:57:21

![day10_8](./img/Day_10.PNG)

## Day11
Stage: 8号競技場

Clear Date: 2022/03/11 14:47:34

![Day_11](./img/Day_11.PNG)

## Day12
Stage: 無秩序な鉱区

Clear Date: 2022/03/12 19:42:10

![day12_8](./img/Day_12.PNG)

## Day13
Stage: 風蝕の高原

多分やってない．．．

## Day14
Stage: 閉鎖監獄

多分やってない．．．