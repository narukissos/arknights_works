# README

ここに危機契約の成果を格納する．

# 危機契約β

公式サイト: https://www.arknights.jp/news/139

開催期間 2020/6/12 〜 6/25 

常設ステージ: 第59区跡

結果：
[通常作戦](./Beta/Seasonal_Permanent_Site/README.md)，
[緊急作戦](./Beta/Rotating_Site/README.md)

# #0 作戦コード「荒廃」

公式サイト: https://www.arknights.jp/news/156

開催期間 2020/09/10 〜 2020/09/23

常設ステージ: 崩壊した大通り

結果：
[通常作戦](./0/Fixed_Site/README.md)，
[緊急作戦](./0/Rotating_Site/README.md)

# #1 作戦コード「黄鉄」

公式サイト:  https://www.arknights.jp/news/163

開催期間 2020/11/11 〜 2020/11/24

常設ステージ: 黄鉄の峡谷

結果：
[通常作戦](./1/Seasonal_Permanent_Site/README.md)，
[緊急作戦](./1/Rotating_Site/README.md)

# #2 作戦コード「利刃」

公式サイト: https://www.arknights.jp/news/173

開催期間 2021/02/04 〜 2021/02/17

常設ステージ: 廃工場

結果：
[通常作戦](./2/Seasonal_Permanent_Site/README.md)，
[緊急作戦](./2/Rotating_Site/README.md)

# #3 作戦コード「灰燼」

公式サイト: https://www.arknights.jp/news/190

開催期間 2021/05/28 〜 2021/06/10

常設ステージ: 風蝕の高原

結果：
[通常作戦](./3/Seasonal_Permanent_Site/README.md)，
[緊急作戦](./3/Rotating_Site/README.md)

# #4 作戦コード「鉛封」

公式サイト: https://www.arknights.jp/news/196

開催期間 2021/07/13 〜 2021/07/26

常設ステージ: 無秩序な鉱区

結果：
[通常作戦](./4/Seasonal_Permanent_Site/README.md)，
[緊急作戦](./4/Rotating_Site/README.md)

# #5 作戦コード「分光」

公式サイト: https://www.arknights.jp/news/208

開催期間 2021/11/11 〜 2021/11/24

常設ステージ: 8号競技場

結果：
[通常作戦](./5/Seasonal_Permanent_Site/README.md)，
[緊急作戦](./5/Rotating_Site/README.md)

# #6 作戦コード「蛮鱗」

公式サイト: https://www.arknights.jp/news/231

開催期間 2022/03/01 〜 2022/03/14

常設ステージ: 狂風の砂原

結果：
[通常作戦](./6/Seasonal_Permanent_Site/README.md)，
[緊急作戦](./6/Rotating_Site/README.md)

# #7 作戦コード「松煙」

公式サイト: https://www.arknights.jp/news/249

開催期間 2022/06/09 〜 2022/06/22

常設ステージ: 灰斉山麓

結果：
[通常作戦](./7/Seasonal_Permanent_Site/README.md)，
[緊急作戦](./7/Rotating_Site/README.md)

# #8 作戦コード「探暁」

公式サイト: https://www.arknights.jp/news/268

開催期間 2022/08/30 〜 2022/09/12

常設ステージ: 大騎士領バー通り

結果：
[通常作戦](./8/Seasonal_Permanent_Site/README.md)，
[緊急作戦](./8/Rotating_Site/README.md)

# #9 作戦コード「深黙」

公式サイト: https://arknights.jp/news/289

開催期間 2022/12/08 〜 2022/12/21

常設ステージ: サルヴィエントの洞窟

結果：
[通常作戦](./9/Seasonal_Permanent_Site/README.md)，
[緊急作戦](./9/Rotating_Site/README.md)

# #10 作戦コード「塵環」

公式サイト: https://arknights.jp/news/305

開催期間 2023/02/03 〜 2023/02/16

常設ステージ: ロンディニウム辺縁区画

結果：
[通常作戦](./10/Seasonal_Permanent_Site/README.md)，
[緊急作戦](./10/Rotating_Site/README.md)

