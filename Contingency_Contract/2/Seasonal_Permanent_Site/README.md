# Contingency Contract #2 Seasonal Permanent Site Results

# MAP

![map](./img/map.PNG)

# Enemy

![enemies](./img/enemy.PNG)

# Results

## Risk 0

Clear Date: 2021/02/04 17:46:57

![0](./img/0.PNG)

## Risk 11

Clear Date: 2021/02/04 18:32:50

![11-1](./img/11-1.PNG)

![11-2](./img/11-2.PNG)

## Risk 15

Clear Date: 2021/02/05 01:25:45

![15-1](./img/15-1.PNG)

![15-2](./img/15-2.PNG)

![15-3](./img/15-3.PNG)

## Risk 17

Clear Date: 2021/02/05 03:14:23

![17-1](./img/17-1.PNG)

![17-2](./img/17-2.PNG)

## Risk18

Clear Date: 2021/02/02 03:31:58

![18-1](./img/18-1.PNG)

![18-2](./img/18-2.PNG)

![18-3](./img/18-3.PNG)

## Risk19

Clear Date: 2021/02/10 02:10:35

![19-1](./img/19-1.PNG)

![19-2](./img/19-2.PNG)

![19-3](./img/19-3.PNG)

## Risk20

Clear Date: 2021/02/11 19:55:30

![20-1](./img/20-1.PNG)

![20-2](./img/20-2.PNG)

![20-3](./img/20-3.PNG)