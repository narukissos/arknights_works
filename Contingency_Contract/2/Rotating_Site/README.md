# Contingency Contract #2 Rotating Site Results

## Day1-2

Stage: 凍てつく廃墟

Clear Date: 2021/02/04 17:39:28

![day1-2](./img/Day_1-2.PNG)

## Day3

Stage: 新通り

Clear Date: 2021/02/06 11:26:24

![day3_8](./img/Day_3.PNG)

## Day4

Stage: 乾荒原

Clear Date: 2021/02/07 21:37:37

![day4_8](./img/Day_4.PNG)

## Day5

Stage: 黄鉄の峡谷

Clear Date: 2021/02/08 20:19:47 

![day5_8](./img/Day_5.PNG)

## Day6

Stage: 崩壊した大通り

Clear Date: 2021/02/09 10:12:09

![day6_8](./img/Day_6.PNG)

## Day7

Stage: 中継所

Clear Date: 2021/02/10 10:09:02

![day7_8](./img/Day_7.PNG)

## Day8
Stage: 武器庫東

Clear Date: 2021/02/11 16:50:35

![day8_8](./img/Day_8.PNG)

## Day9
Stage: 乾荒原

Clear Date: 2021/02/12 13:27:15

![day9_8](./img/Day_9.PNG)

## Day10
Stage: 新通り

Clear Date: 2021/02/13 12:15:12

![day10_8](./img/Day_10.PNG)

## Day11
Stage: 黄鉄の峡谷

Clear Date: 2021/02/14 10:51:21

![Day_11](./img/Day_11.PNG)

## Day12
Stage: 中継所

Clear Date: 2021/02/05 15:53:30

![day12_8](./img/Day_12.PNG)

## Day13
Stage: 中継所

Clear Date: 2021/02/16 12:18:20

![day13_8](./img/Day_13.PNG)

## Day14
Stage: 凍てつく廃墟

Clear Date: 2021/02/17 12:29:49

![day14_8](./img/Day_14.PNG)