# Contingency Contract #5 Seasonal Permanent Site Results

# MAP

![map](./img/maps.PNG)

## Enemy

![enemies](./img/enemies.PNG)

# Results

## Risk 0

Clear Date: 2021/11/11 19:20:19

![0](./img/0.PNG)

## Risk 10

Clear Date: 2021/05/29 04:02:20

![10-1](./img/10-1.PNG)

## Risk 14

Clear Date: 2021/11/11 21:40:15

![14-1](./img/14-1.PNG)
![14-2](./img/14-2.PNG)

## Risk 16

Clear Date: 2021/11/11 21:49:47

![16-1](./img/16-1.PNG)
![16-1](./img/16-2.PNG)

## Risk 17

Clear Date: 2021/22/19 22:19:40

![17-1](./img/17-1.PNG)
![17-2](./img/17-2.PNG)

## Risk 18

Clear Date: 2021/11/11 22:50:15

![18-1](./img/18-1.PNG)
![18-2](./img/18-2.PNG)

## Risk 19

Clear Date: 2021/11/11 23:02:16

![19-1](./img/19-1.PNG)
![19-2](./img/19-2.PNG)

# 特別任務

## 任務1
![challenge1](./img/Challenge_1.PNG)

## 任務2
![challenge2](./img/Challenge_2.PNG)

## 任務3
![challenge3](./img/Challenge_3.PNG)

## 任務4
![challenge4](./img/Challenge_4.PNG)

## 任務5
![challenge5](./img/Challenge_5.PNG)

## 任務6
![challenge6](./img/Challenge_6.PNG)

## 任務7
![challenge7](./img/Challenge_7.PNG)

## 任務8
![challenge8](./img/Challenge_8.PNG)