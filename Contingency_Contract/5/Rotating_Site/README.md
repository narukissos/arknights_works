# Contingency Contract #5 Rotating Site Results

## Day1-2

Stage: 武器庫東

Clear Date: 2021/11/11 17:56:28

![day1-2](./img/Day_1-2.PNG)

## Day3

Stage: 風蝕の高原

Clear Date: 2021/11/13 17:27:11

![day3_8](./img/Day_3.PNG)

## Day4

Stage: 廃工場

Clear Date: 2021/11/14 13:15:40

![day4_8](./img/Day_4.PNG)

## Day5

Stage: 黄鉄の峡谷

Clear Date: 2021/11/15 20:28:57 

![day5_8](./img/Day_5.PNG)

## Day6

Stage: 第6区跡

Clear Date: 2021/11/16 23:04:13

![day6_8](./img/Day_6.PNG)

## Day7

Stage: 閉鎖監獄

Clear Date: 2021/11/17 12:50:33

![day7_8](./img/Day_7.PNG)

## Day8
Stage: 無秩序な鉱区

Clear Date: 2021/11/18 20:25:12

![day8_8](./img/Day_8.PNG)
![day8_13](./img/Day_8-2.PNG)

## Day9
Stage: 黄鉄の峡谷

Clear Date: 2021/11/19 20:47:38

![day9_8](./img/Day_9.PNG)

## Day10
Stage: 第6区跡

Clear Date: 2021/11/16 10:19:33

![day10_8](./img/Day_10.PNG)

## Day11
Stage: 廃工場

Clear Date: 2021/11/21 08:01:58

![Day_11_8](./img/Day_11.PNG)
![Day_11_12](./img/Day_11-2.PNG)

## Day12
Stage: 閉鎖監獄

Clear Date: 2021/11/22 08:45:30

![day12_8](./img/Day_12.PNG)

## Day13
Stage: 風蝕の高原

Clear Date: 2021/11/23 10:40:49

![day13_8](./img/Day_13.PNG)

## Day14
Stage: 無秩序な鉱区

Clear Date: 2021/11/24 22:39:31

![day14_8](./img/Day_14.PNG)
