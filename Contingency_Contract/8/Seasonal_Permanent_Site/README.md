# Contingency Contract #8 Seasonal Permanent Site Results

# Results

## Risk 11

Clear Date: 2022/08/31 19:52:28

![11-1](./img/11-1.PNG)
![11-1](./img/11-2.PNG)

## Risk 15

Clear Date: 2022/08/31 20:30:44

![15-1](./img/15-1.PNG)
![15-2](./img/15-2.PNG)

## Risk 16

Clear Date: 2021/08/31 21:01:28

![16-1](./img/16.PNG)

## Risk 18

Clear Date: 2022/08/31 21:16:46

![18-1](./img/18-1.PNG)

![18-2](./img/18-2.PNG)
