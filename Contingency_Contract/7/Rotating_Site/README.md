# Contingency Contract #7 Rotating Site Results

## Day1-2

Stage: 灼熱の溶岩洞

Clear Date: 2022/06/09 17:10:09

![day1-2](./img/Day_1-2.PNG)

## Day3

Stage: 風蝕の高原

Clear Date: 2022/06/11 09:23:43

![day3_8](./img/Day_3.PNG)

## Day4

Stage: 閉鎖監獄

Clear Date: 2022/06/12 09:39:17

![day4_8](./img/Day_4.PNG)

## Day5

Stage: 棄てられし区画

Clear Date: 2022/06/13 11:16:30 

![day5_8](./img/Day_5.PNG)
![day5_15](./img/Day_5-2.PNG)

## Day6

Stage: 無秩序な鉱区

Clear Date: 2022/06/14 16:22:52

![day6_8](./img/Day_6.PNG)
![day6_13](./img/Day_6-2.PNG)

## Day7

Stage: 8号競技場

Clear Date: 2022/06/15 18:02:54

![day7_8](./img/Day_7.PNG)

## Day8
Stage: 狂風の砂原

Clear Date: 2022/06/16 18:48:54

![day8_8](./img/Day_8.PNG)

## Day9
Stage: 風蝕の高原

Clear Date: 2022/06/17 20:32:29

![day9_8](./img/Day_9.PNG)

## Day10
Stage: 狂風の砂原

Clear Date: 2022/06/18 04:34:52

![day10_8](./img/Day_10.PNG)

## Day11
Stage: 棄てられし区画

Clear Date: 2022/06/19 09:58:28

![Day_11](./img/Day_11.PNG)

## Day12
Stage: 8号競技場

Clear Date: 2022/06/20 11:30:30

![day12_8](./img/Day_12.PNG)

## Day13
Stage: 無秩序な鉱区

Clear Date: 2022/06/21 12:35:27

![day13_8](./img/Day_13.PNG)

## Day14
Stage: 灼熱の溶岩洞

Clear Date: 2022/06/22 11:44:37

![day14_8](./img/Day_14.PNG)
