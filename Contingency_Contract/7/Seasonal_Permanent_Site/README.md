# Contingency Contract #7 Seasonal Permanent Site Results

# Results

## Risk 8

Clear Date: 2022/06/09 22:37:16

![8](./img/8.PNG)

## Risk 11

Clear Date: 2022/06/09 22:53:10

![11](./img/11.PNG)

## Risk 13

Clear Date: 2022/06/09 23:00:49

![13-1](./img/13.PNG)
![13-2](./img/13-2.PNG)

## Risk 15

Clear Date: 2022/06/09 23:11:59

![15-1](./img/15.PNG)
![15-1](./img/15-2.PNG)

## Risk 16

Clear Date: 2022/06/09 23:47:15

![16-1](./img/16.PNG)
![16-2](./img/16-2.PNG)

## Risk 17

Clear Date: 2022/06/10 00:00:04

![17-1](./img/17.PNG)
![17-2](./img/17-2.PNG)

## Risk 18

Clear Date: 2022/06/10 00:36:44

![18-1](./img/18.PNG)
![18-2](./img/18-2.PNG)

## Risk 20

Clear Date: 2022/06/14 21:30:45

![20-1](./img/20.PNG)
![20-2](./img/20-2.PNG)

## Risk 23

Clear Date: 2022/06/14 23:56:29

![23-1](./img/23.PNG)
![23-2](./img/23-2.PNG)

## Risk 24

Clear Date: 2022/06/14 23:56:29

![24-1](./img/24.PNG)
![24-2](./img/24-2.PNG)


# 特別任務

## 任務1
![challenge1](./img/challange_1.PNG)
