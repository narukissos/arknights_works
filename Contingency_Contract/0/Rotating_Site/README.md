# Contingency Contract #0 Routing Site Results

## Day1-2

Stage: 乾荒原

Clear Date: 2020/09/10 19:29:15

![day1-2_8](./img/day1-2_8.PNG)

## Day3

Stage: 新通り

Clear Date: 2020/09/12 11:50:23

![day3_8](./img/day3_8.PNG)

## Day4

Stage: 荒廃した広場

Clear Date: 2020/09/13 07:36:31

![day4_8](./img/day4_8.PNG)



## Day5

Stage: 中継所

Clear Date: 2020/09/14 21:17:00

![day5_8](./img/day5_8.PNG)

## Day6

Stage: 凍てつく廃墟

Clear Date: 2020/09/15 20:40:09

![day6_8](./img/day6_8.PNG)

## Day7

Stage: 無人廃ビル

Clear Date: 2020/09/16 12:56:10

![day7_8](./img/day7_8.PNG)

## Day8
Stage: 第59区跡

Clear Date: 2020/09/17 07:55:38

![day8_8](./img/day8_8.PNG)

## Day9
Stage: 乾荒原

Clear Date: 2020/09/18 07:59:57

![day9_8](./img/day9_8.PNG)

## Day10
Stage: 新通り

Clear Date: 2020/09/19 07:45:06

![day10_8](./img/day10_8.PNG)

## Day11
Stage: 無人廃ビル

Clear Date: 2020/09/20 09:42:26

![day11_8](./img/day11_8.PNG)

## Day12
Stage: 中継所

Clear Date: 2020/09/21 12:06:29

![day12_8](./img/day12_8.PNG)

## Day13
Stage: 凍てつく廃墟

Clear Date: 2020/09/22 07:00:22

![day13_8](./img/day13_8.PNG)

## Day14
Stage: 第59区跡

Clear Date: 2020/09/23 09:24:32

![day14_8](./img/day14_8.PNG)