# Contingency Contract #0 Fixed Site Results

## Risk 10

Clear Date: 2020/09/10 22:12:06

![10](./img/10.PNG)

## Risk 11

Clear Date: 2020/09/11 00:28:09

![11-1](./img/11-1.PNG)

![11-2](./img/11-2.PNG)

## Risk 12

Clear Date: 2020/09/11 13:02:44

![12-1](./img/12-1.PNG)

![12-2](./img/12-2.PNG)

## Risk 13

Clear Date: 2020/09/13 10:50:53

![13-1](./img/13-1.PNG)

![13-2](./img/13-2.PNG)

## Risk14

Clear Date: 2020/09/16 20:58:56

![14-1](./img/14-1.PNG)

![14-2](./img/14-2.PNG)

## Risk15

Clear Date: 2020/09/17 19:46:47

![15-1](./img/15-1.PNG)

![15-2](./img/15-2.PNG)

## Risk16

Clear Date: 2020/09/17 19:59:46

![16-1](./img/16-1.PNG)

![16-2](./img/16-2.PNG)

## Risk17

Clear Date: 2020/09/17 20:07:49

![17-1](./img/17-1.PNG)

![17-2](./img/17-2.PNG)

## Risk18

Clear Date: 2020/09/17 20:30:30

![18-1](./img/18-1.PNG)

![18-2](./img/18-2.PNG)

## Risk19

Clear Date: 2020/09/17 23:28:12

![19-1](./img/19-1.PNG)

![19-2](./img/19-2.PNG)

## Risk20

Clear Date: 2020/09/21 12:59:08

![20-1](./img/20-1.PNG)

![20-2](./img/20-2.PNG)

## Risk21

Clear Date: 2020/09/22 18:15:32

![21-1](./img/21-1.PNG)

![21-2](./img/21-2.PNG)