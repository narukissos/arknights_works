# Contingency Contract #4 Seasonal Permanent Site Results

# Results

## Risk 9

Clear Date: 2021/07/13 22:48:25

![9-1](./img/9-1.PNG)

![9-2](./img/9-2.PNG)

## Risk 11

Clear Date: 2021/07/13 23:05:18

![11-1](./img/11-1.PNG)

## Risk 12

Clear Date: 2021/07/13 23:31:46

![12-1](./img/12-1.PNG)

## Risk 18

Clear Date: 2021/07/14 21:58:13

![18-1](./img/18-1.PNG)

![18-2](./img/18-2.PNG)