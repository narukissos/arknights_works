# Contingency Contract #4 Rotating Site Results

## Day1-2

Stage: 閉鎖監獄

Clear Date: 2021/07/13 22:13:56

![day1-2](./img/Day_1-2.PNG)

## Day3

Stage: 中継所

Clear Date: 2021/07/15 19:37:14

![day3_8](./img/Day_3.PNG)

## Day4

Stage: 武器庫東

Clear Date: 2021/07/16 22:50:16

![day4_8](./img/Day_4.PNG)

## Day5

Stage: 黄鉄の峡谷

Clear Date: 2021/07/17 08:49:15 

![day5_8](./img/Day_5.PNG)

## Day6

Stage: 第6区跡

Clear Date: 2021/07/18 10:12:23

![day6_8](./img/Day_6.PNG)

## Day7

Stage: 風蝕の高原

Clear Date: 2021/07/19 20:55:24

![day7_8](./img/Day_7.PNG)

## Day8
Stage: 廃工場

Clear Date: 2021/07/20 22:23:09

![day8_8](./img/Day_8.PNG)

## Day9
Stage: 黄鉄の峡谷

Clear Date: 2021/07/21 22:47:43

![day9_8](./img/Day_9.PNG)

## Day10
Stage: 閉鎖監獄

Clear Date: 2021/07/22 11:12:15

![day10_8](./img/Day_10.PNG)

## Day11
Stage: 中継所

Clear Date: 2021/07/23 09:53:52

![Day_11](./img/Day_11.PNG)

## Day12
Stage: 風蝕の高原

Clear Date: 2021/07/24 10:05:41

![day12_8](./img/Day_12.PNG)

## Day13
Stage: 第6区跡

Clear Date: 2021/10/40 10:40:36

![day13_8](./img/Day_13.PNG)

## Day14
Stage: 凍てつく廃墟

Clear Date: 2021/07/27 01:36:55

![day14_8](./img/Day_14.PNG)
