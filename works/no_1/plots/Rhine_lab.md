TODO

・ネタ集め
　→ライン生命の倫理規範
　
目標
　→元ネタを見つけ，草稿における該当部分を完成させる

仮説：ライン生命はマックスプランク研究所をモチーフにしている
→マックス・プランク研究所の医療部門などのHPを漁るしかない

とりあえず今まで見つかったもの
・Rules of Good Scientific Practice
https://www.mpimp-golm.mpg.de/92547/Rules-of-Good-Scientific-Practice_MPG.pdf

・Guidelines and Rules of the Max Planck Societyon a Responsible Approach to Freedom of Research and Research Risks(2017)
https://www.mpg.de/197392/researchFreedomRisks.pdf

↓ネタとなるような記載の抜粋
倫理的に責任のある研究に関する原理
【一般原理】
Scientists must therefore prevent or minimize direct or indirect harm to humans and the environment as far as possible.

【リスク分析】
As far as possible, researchers should therefore take account of the consequences and opportunities for application and misuse of their work and its controllability. 

research projects that are potentially susceptible to risk should therefore be preceded by an evaluation of the associated risks to human dignity, human life and human welfare, the environment and any other significant values protected under the constitution

【リスク最小化】
Researchers and all other persons involved should minimize, as far as possible, the risks associated with the implementation or use of their work to human dignity, life, welfare, freedom and property, and to the protection of the environment. 

【出版】
The possible consequences of publication of results in high-risk research areas should be evaluated responsibly and at an early stage

In contrast, suppression of research results may prevent effective protection against their misuse by totalitarian regimes, terrorist groups, organized criminal groups or individual criminals.

【Foregoing irresponsible research as ultima ratio】
ultima ratio = final argument
日本語訳無理ー
最後通牒としての前述の無責任な研究？？？？
→Foregoingが何を指しているのか？
→マックス・プランク研究所は前身となるビルヘルム財団なんたらのやつで人体実験とか毒ガス実験やってたけど…

リスト化して項目名を隠す方向性で
この節での主張は？
ロジック
　・科学者による責任ある意思決定がultima ratioとなりうる
　・潜在的リスクが不釣り合い（何に？）あるいは制限できない状況において研究者の責任ある意思決定が遂行されないかもしれない．
　・頑張って研究結果が含意するハームフルな影響も見積もってね（意思決定の状況においても＾＾）．

【トレーニングと情報】
the principles of a responsible approach to research risks should be communicated and an example should be set. 

