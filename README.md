# Arknights_works

アークナイツに関する情報．創作物をここに配置する．

# Arknightsリンク集

## Arknights公式

* [Arknights Official Web Site(Japanese)](https://www.arknights.jp/)
* [Arknights Official Web Site(Chinese)](https://ak.hypergryph.com/index)
* [Arknights Official Web Site(English)](https://www.arknights.global/)
* [Arknights Twitter JP](https://twitter.com/ArknightsStaff)
* [Arknights Twitter EN](https://twitter.com/ArknightsEN)
* [Arknights Twitter Korea](https://twitter.com/ArknightsKorea)
* [Arknights Official Instagram](https://www.instagram.com/arknights_messenger_official/)
* [Arknights Official Facebook](https://www.facebook.com/ArknightsGlobal)
* [Arknights Global Official Youtube Channel](https://www.youtube.com/channel/UCR0J2NYGuC8epsa1O4DMmXQ)
* [Arknights Japaese Official Youtube Channel](https://www.youtube.com/channel/UCvoQlzEzqa6vQA8hq9GNNug)
* [Weibo Arknights Page](https://www.weibo.com/arknights?is_all=1#_loginLayer_1587822669745)
* [bilibili Arknights Page](https://space.bilibili.com/161775300/)
* [Yostar Official Shop Arknights](https://shop.yostar.co.jp/products/list?category_id=MTExODQ3OTU=)
* [Yostar Official Shop Arknights EN](https://yostar.store/collections/arknights)
* [Monster Silen.hypergryph](https://monster-siren.hypergryph.com/music/232208)
* [Arknights Official Anime Site](https://arknights-anime.jp/)

## Arknights 攻略

* [アークナイツ攻略Wiki](https://arknights.wikiru.jp/index.php?%A5%A2%A1%BC%A5%AF%A5%CA%A5%A4%A5%C4%B9%B6%CE%AC%20Wiki)
* [Arknights wiki JP](https://wiki3.jp/arknightsjp)
* [Arknights Toolbox](https://aceship.github.io/AN-EN-Tags/index.html)
* [Penguin Statistics](https://penguin-stats.io/)
* [Arknights Maps](https://map.ark-nights.com/map)
* [Discord Arknights global Server](https://discordapp.com/channels/586503387072167937/586589326813429800)
* [Discord Arknights JP Server](https://discordapp.com/channels/602033874217730058/666987081897213953)
